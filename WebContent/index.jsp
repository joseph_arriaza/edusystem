<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page session="true" %>    
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="mobile-web-app-capable" content="yes">
	  	<meta name="apple-mobile-web-app-capable" content="yes">
	  	<meta charset="UTF-8">
	  	
		<title>Life in Code</title>
	</head>
	<body > 
		<%
			HttpSession miSesion=request.getSession();
			Object user = miSesion.getAttribute("usuario");
			if(user!=null){
			 %>
				<jsp:forward page="usuario/dashboard.jsp" >
					<jsp:param name="estado" value="Ya haz accedido al sistema."/>
				</jsp:forward>
			<%}
		%>
		<form method="post" action="ServletAutenticar.do">
			Usuario:
			<input type="text" name="txtUsuario" value="${usuarioTxt}" required>
			Contraseņa:
			<input type="password" name="txtPassword" required>
			<script>
				var path=window.location.href;ruta=path.split("/");
				document.write('<input type="hidden" name="ruta" value="'+ruta[0]+"/"+ruta[1]+"/"+ruta[2]+"/"+ruta[3]+"/"+'" required>');
			</script>
			<input type="submit" value="Iniciar sesion">
		</form>
		<br><br><br>
		<form method="post" action="ServletRegistrar.do">
			Nombre de Usuario:
			<br><input type="text" name="txtNombreUsuario" required><br>
			Contraseņa:
			<br><input type="password" name="txtContrasenia" required><br>
			Repite la contraseņa:
			<br><input type="password" name="txtContrasenia2" required><br>
			Nombre:
			<br><input type="text" name="txtNombre" value="${nombreU}" required><br>
			Apellido:
			<br><input type="text" name="txtApellido" value="${apellidoU}" required><br>
			E-mail:
			<br><input type="email" name="txtCorreo" value="${correoU}" required><br>
			Direccion:
			<br><input type="text" name="txtDireccion" value="${direccionU}" required><br>
			Escoje tu tipo de cuenta:
			<br><input type="radio" name="txtCuenta" value="2" checked>Alumno
			<br><input type="radio" name="txtCuenta" value="3">Profesor<br>
			<script>
				var path=window.location.href;ruta=path.split("/");
				document.write('<input type="hidden" name="ruta" value="'+ruta[0]+"/"+ruta[1]+"/"+ruta[2]+"/"+ruta[3]+"/"+'" required>');
			</script>
			<br><input type="submit" value="Registrarse">
		</form>
		<script>
			if("${estado}"!=""){
				alert("${estado}");
			}
		</script>
	</body>
</html>