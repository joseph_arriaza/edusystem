<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProf.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<form method="post" action="ServletAgregarExamen.do">
		Nombre Examen:<br>
		<input type="hidden" name="estadoAE" value="true" required>
		<input type="hidden" name="idCurso" value="${cursoN.getIdCurso()}" required>
		<input type="hidden" name="idTema" value="${temaN.getIdTema()}" required>
		<input type="hidden" name="idGrupo" value="${grupoN.getIdGrupo()}" required>
		<input type="text" name="txtNombreExamen" required><br>
		<input type="submit" value="Crear examen">
	</form>
</body>
</html>