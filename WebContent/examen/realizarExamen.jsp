<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionAl.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	  <c:set var="numeroRespuesta" value="${cantidadPreguntas}" />
	  <br><br><br>
	  Examen: ${examenN.getNombreExamen()}<br><br>
	  <form method="post" action="ServletRealizarExamen.do">
	  	 <input type="hidden" name="txtIdExamen" value="${examenN.getIdExamen()}" required>
	  	 <input type="hidden" name="txtIdCurso" value="${idCurso}" required>
	  	 <input type="hidden" name="realizado" value="true" required>
	  	 <input type="hidden" name="campos" value="${cantidadPreguntas}" required>
		 <c:forEach var="preguntaR" items="${listaPreguntas}">
		   <strong>${preguntaR.getNombrePregunta()}</strong><br><br>
		  	<input type="text" name="${numeroRespuesta}" required><br><br>
		   	<c:set var="numeroRespuesta" value="${numeroRespuesta-1}" />
		  </c:forEach>
		  <br><input type="submit" value="Enviar respuestas">
	  </form>
</body>
</html>