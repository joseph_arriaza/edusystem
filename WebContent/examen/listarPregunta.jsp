<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProfAl.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<br><br>
	Grupo: ${grupoN.getNombreGrupo()}<br><br>
	<a href="ServletAlumnoGrupo.do?idGrupo=${idG}">Alumnos por Curso</a><br>
	<a href="ServletAgregarCurso.do?idGrupo=${idG}">Agregar Curso</a><br><br><br>
	<a href="ServletListarCurso.do?idGrupo=${grupoN.getIdGrupo()}">Curso</a>: ${cursoN.getNombreCurso()}<br><a href="ServletEliminarCurso.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarCurso.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Editar </a><br>
	<a href="ServletAgregarTema.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}">Agregar Tema</a><br><br>
	<br><a href="ServletListarTema.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${grupoN.getIdGrupo()}">Tema</a>: ${temaN.getNombreTema()}<br>
	<a href="ServletEliminarTema.do?idTema=${tema.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarTema.do?idTema=${tema.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Editar </a>
	<br>
	<a href="ServletAgregarExamen.do?idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}">Agregar Examen</a><br><br><br>
	<a href="ServletListarExamen.do?idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}">Examen</a>: ${examenN.getNombreExamen()}<br>
	<a href="ServletAgregarPregunta.do?idExamen=${examenN.getIdExamen()}&idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Agregar Pregunta </a><br>
	<a href="ServletEliminarExamen.do?idExamen=${examenN.getIdExamen()}&idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarExamen.do?idExamen=${examen.getIdExamen()}&idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Editar </a>
	<br><br><br>
	<c:forEach var="pregunta" items="${listaPreguntas}">
		Pregunta &nbsp;: ${pregunta.getNombrePregunta()}<br>
		Respuesta: ${pregunta.getRespuestaPregunta()}<br>
		Punteo &nbsp;&nbsp;&nbsp;&nbsp;: ${pregunta.getPunteoPregunta()}<br>
		<a href="ServletEliminarPregunta.do?idPregunta=${pregunta.getIdPregunta()}&idExamen=${examenN.getIdExamen()}&idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarPregunta.do?idPregunta=${pregunta.getIdPregunta()}&idExamen=${examenN.getIdExamen()}&idTema=${temaN.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Editar </a>	
		<br><br><br>
	</c:forEach>
</body>
</html>