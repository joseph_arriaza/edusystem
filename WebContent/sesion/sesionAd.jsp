<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.edusystem.beans.Usuario" %> 
<%@ page session="true" %> 

<%
	HttpSession miSesion=request.getSession();
	Usuario user = (Usuario)miSesion.getAttribute("usuario");
	if(user==null || (user!=null & user.getRol().getIdRol()!=1)){
	 %>
		<jsp:forward page="${ruta}/index.jsp" >
			<jsp:param name="estado" value="Para Acceder al Sistema, por favor Inicie Sesi�n."/>
		</jsp:forward>
	<%}
%>