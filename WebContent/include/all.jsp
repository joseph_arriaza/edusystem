<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Live in Code</title>
</head>
<body>
	<c:set var="user" scope="session" value="${usuario}"/>
	<img src="${ruta}images/usuario/${rolUsuario.getFoto()}" width="130px" height="130px"/>
	<br>Hola ${rolUsuario.getNombre()}<br>
	<a href="${ruta}index.jsp">Inicio </a>
	<a href="${ruta}ServletLogout.do"> Cerrar Sesion </a>
	<c:if test="${user.getRol().getIdRol() == 1}">
		<a href="ServletAgregarUsuario.do">Agregar Administrador</a><br><br>
	  	<form method="post" action="ServletBuscarGrupo.do">
			<input type="text" placeholder="Nombre del grupo" name="txtNombreGrupo">
			<input type="submit" value="Buscar">
		</form><br>
		<form method="post" action="ServletBuscarUsuario.do">
			<input type="text" placeholder="Nombre del usuario" name="txtUsernameBuscar">
			<input type="submit" value="Buscar">
		</form><br>
	</c:if>
	<c:if test="${user.getRol().getIdRol() == 2}">
		&nbsp;&nbsp;&nbsp;&nbsp;<a href="${ruta}upload/uploadForm.jsp"> Sube tu foto</a>
		<form method="post" action="ServletBuscarGrupo.do">
			<input type="text" placeholder="Nombre del grupo" name="txtNombreGrupo">
			<input type="submit" value="Buscar">
		</form>
	  	<br><br><br>
	  	Lista de Grupos:<br>
	  	<c:forEach var="grupo" items="${listaGrupoAl}">	
	  		Grupo: <a href="${ruta}ServletListarCurso.do?idGrupo=${grupo.getIdGrupo()}">${grupo.getNombreGrupo()}</a><br>
	  	</c:forEach>
	</c:if>
	<c:if test="${user.getRol().getIdRol() == 3}">
		<a href="${ruta}group/add.jsp">Crear Grupo</a>
		&nbsp;&nbsp;&nbsp;&nbsp;<a href="${ruta}upload/uploadForm.jsp"> Sube tu foto</a>
	  	<br><br><br>
	  	Lista de Grupos:<br>
	  	<c:forEach var="grupo" items="${listaGrupo}">	
	  		<a href="${ruta}ServletListarCurso.do?idGrupo=${grupo.getIdGrupo()}">${grupo.getNombreGrupo()}</a>|<a href="${ruta}ServletRemoveGroup.do?idGrupo=${grupo.getIdGrupo()}">Eliminar</a>|<a href="${ruta}ServletEditGroup.do?idGrupo=${grupo.getIdGrupo()}">Editar</a>
	  		| <a href="${ruta}ServletVerSolicitudes.do?idGrupo=${grupo.getIdGrupo()}"> Ver Solicitudes</a><br>
	  	</c:forEach>
	</c:if>
	
	<script>
		if("${estado}"!=""){
			alert("${estado}");
		}
	</script>
</body>
</html>