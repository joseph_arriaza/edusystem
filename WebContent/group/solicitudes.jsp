<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProfAd.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<br><br><br>
	Grupo: ${grupoN.getNombreGrupo()}<br><br>
	<c:forEach var="solicitud" items="${listaSolicitud}">
		Nombre Alumno : <strong> ${solicitud.getAlumno().getNombreAlumno()} </strong><br>
		Fecha Solicitud: <strong> ${solicitud.getFechaSolicitud()} </strong><br>
		<a href="ServletActualizarSolicitud.do?idSolicitud=${solicitud.getIdSolicitud()}&estadoS=true"> Aceptar Solicitud </a> | <a href="ServletActualizarSolicitud.do?idSolicitud=${solicitud.getIdSolicitud()}&estadoS=false"> Cancelar Solicitud </a> 
		<br><br>
	</c:forEach>
</body>
</html>