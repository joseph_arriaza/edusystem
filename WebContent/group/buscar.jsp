<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionAlAd.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<br><br><br>
	Lista de Grupos(No asignados):
	<br><br>
	<c:forEach var="lgB" items="${listaGruposBusqueda}">
		${lgB.getNombreGrupo()} 
		<c:if test="${user.getRol().getIdRol() == 2}">
	  		<a href="ServletSolicitudGrupo.do?idGrupo=${lgB.getIdGrupo()}"> Enviar Solicitud</a>
		</c:if>
		<c:if test="${user.getRol().getIdRol() == 1}">
	  		<a href="ServletVerSolicitudes.do?idGrupo=${lgB.getIdGrupo()}&admin=admin"> Ver Solicitudes</a>
		</c:if>
		<br>
	</c:forEach>
</body>
</html>