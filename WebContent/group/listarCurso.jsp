<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProfAl.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<c:set var="user" scope="session" value="${usuario}"/>
	<br><br>
	<c:if test="${user.getRol().getIdRol() == 2}">
		<br>
	</c:if>
	Grupo: ${grupoN.getNombreGrupo()}<br><br>
	<c:if test="${user.getRol().getIdRol() == 3}">
	  	<a href="ServletAlumnoGrupo.do?idGrupo=${idG}">Alumnos por Curso</a><br>
		<a href="ServletAgregarCurso.do?idGrupo=${idG}">Agregar Curso</a><br><br><br>
	</c:if>
	<c:forEach var="curso" items="${listaCursos}">
		Curso: ${curso.getNombreCurso()}<br>
		Descripcion: ${curso.getDescripcion()}<br>
		Categoria: ${curso.getCategoria().getNombreCategoria()}<br>
		Fecha Creacion: ${curso.getFechaPublicacion()}<br>
		<a href="ServletListarTema.do?idCurso=${curso.getIdCurso()}&idGrupo=${grupoN.getIdGrupo()}">Temas</a><br>
		<c:if test="${user.getRol().getIdRol() == 1}">
			<a href="ServletEliminarCurso.do?idCurso=${curso.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarCurso.do?idCurso=${curso.getIdCurso()}&idGrupo=${idG}"> Editar </a><br><br><br>
		</c:if>
	</c:forEach>
</body>
</html>