<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProfAl.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<br><br><br><br>
	Grupo: ${grupo.getNombreGrupo()}
	<br><br>
	<form method="post" action="ServletBuscarAlumno.do">
		<input type="text" name="txtUsernameAl" placeholder="Buscar nombre de usuario del alumno" required>
		<input type="hidden" name="idGrupo" value="${grupo.getIdGrupo()}" required>
		<input type="submit" value="Buscar">
	</form>
	<br><br>
	<c:forEach var="alGp" items="${listaAG}">
		<img src="${ruta}images/usuario/${alGp.getFotoAlumno()}" width="90px" height="90px"/><br>
		Username: ${alGp.getUsuario().getNombreUsuario()}<br>		
		Nombre: ${alGp.getNombreAlumno()}<br>
		Apellido: ${alGp.getApellidoAlumno()}<br>
		Correo : ${alGp.getCorreoAlumno()}<br>
		<a href="ServletEliminarAlumnoCurso.do?idAlumno=${alGp.getIdAlumno()}&idGrupo=${grupo.getIdGrupo()}">Eliminar alumno</a><br><br>
	</c:forEach>
	<c:forEach var="alumno" items="${listaAl}">
		<img src="${ruta}images/usuario/${alumno.getFotoAlumno()}" width="90px" height="90px"/><br>
		Username: ${alumno.getUsuario().getNombreUsuario()}<br>		
		Nombre: ${alumno.getNombreAlumno()}<br>
		Apellido: ${alumno.getApellidoAlumno()}<br>
		<a href="ServletAgregarAlumnoCurso.do?idAlumno=${alumno.getIdAlumno()}&idGrupo=${grupo.getIdGrupo()}">Agregar alumno</a><br><br>
	</c:forEach>
</body>
</html>