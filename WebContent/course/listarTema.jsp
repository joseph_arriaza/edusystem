<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProfAl.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<br><br>
	<c:if test="${user.getRol().getIdRol() == 2}">
		<br>
	</c:if>
	Grupo: ${grupoN.getNombreGrupo()}<br><br>
	<c:if test="${user.getRol().getIdRol() == 3}">
		<a href="ServletAlumnoGrupo.do?idGrupo=${idG}">Alumnos por Curso</a><br>
		<a href="ServletAgregarCurso.do?idGrupo=${idG}">Agregar Curso</a><br><br><br>
	</c:if>
	<a href="ServletListarCurso.do?idGrupo=${idG}">Curso</a>: ${cursoN.getNombreCurso()}
	<c:if test="${user.getRol().getIdRol() == 3}">
		<br><a href="ServletEliminarCurso.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarCurso.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Editar </a><br>
		<a href="ServletAgregarTema.do?idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}">Agregar Tema</a>
		<br>
	</c:if>
	<br><br>
	<c:forEach var="tema" items="${listaTema}">
		Nombre Tema: ${tema.getNombreTema()}
		<br><a href="ServletListarExamen.do?idTema=${tema.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}">Examenes</a><br>
		<c:if test="${user.getRol().getIdRol() == 3}">
			<a href="ServletEliminarTema.do?idTema=${tema.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Eliminar </a>  |  <a href="ServletEditarTema.do?idTema=${tema.getIdTema()}&idCurso=${cursoN.getIdCurso()}&idGrupo=${idG}"> Editar </a>
		</c:if>
		<br><br>
	</c:forEach>
</body>
</html>