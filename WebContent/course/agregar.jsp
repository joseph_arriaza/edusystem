<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="/sesion/sesionProf.jsp"></jsp:include>
<jsp:include page="/include/all.jsp"></jsp:include>
<body>
	<form method="post" action="ServletAgregarCurso.do">
		Nombre Curso:<br>
		<input type="hidden" name="estadoAC" value="true" required>
		<input type="hidden" name="idGrupo" value="${idGrupo}" required>
		<input type="text" name="txtNombreCurso" required><br>
		Descripcion:<br>
		<input type="text" name="txtDescripcion" required><br>
		<select name="txtCategoria" required>
			<c:forEach var="cate" items="${cat}">
				<option value="${cate.getIdCategoria()}">${cate.getNombreCategoria()}</option>
			</c:forEach>
		</select>
		<input type="submit" value="Crear curso">
	</form>
</body>
</html>