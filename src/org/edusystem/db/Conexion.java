package org.edusystem.db;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.stat.Statistics;
import org.edusystem.beans.Usuario;
/**
 * Clase que nos brinda la conexi�n y funciones a la base de datos
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class Conexion {
	private SessionFactory session;
	private static Conexion instancia = null;
	private static Statistics estadisticas;
	/**
     * Metodo que hace que la instancia de la clase sea nula
     */
	public  void setInstancia(){
		this.instancia=null;
	}
	/**
     * Metodo que hace una nueva instancia de la Conexion
     */
	public Conexion() {
		try {
			session = new Configuration().configure().buildSessionFactory();
			estadisticas=session.getStatistics();
			estadisticas.setStatisticsEnabled(true);
			
			System.out.println("Configuracion finalizada");
		} catch (Throwable ex) {
			System.out.println("Mensaje de ERROR");
			ex.getMessage();
			System.out.println("Traza de error");
			ex.printStackTrace();
			System.out
					.println("Error al establecer conexion con motor de base de datos");
		}
	}
	
    /**
     * Cierra la sesion.
     */
    public void closeSession()throws HibernateException {
        try {
        	
            if (session.isClosed()==false | session.getCurrentSession().isOpen()) {
            	session.close();
            	session.getCurrentSession().close();
            }
        } catch (HibernateException ex) {
            throw new HibernateException(ex);
        }
    }

    /**
     * Metodo que nos da las estadisticas
     * @return Las estadisticas de la conexion
     */
	public Statistics getEstadisticas(){
		return this.estadisticas;
	}
	 /**
     * Metodo que nos da la instancia de la clase, si es nula, hace una nueva instancia por el contrario nos devuelve la ya creada
     * @return La instancia de la clase
     */
	public synchronized static Conexion getInstancia() {
		if (instancia == null) {
			instancia = new Conexion();
		}
		return instancia;
	}
	/**
     * Metodo que nos da la sesion
     * @return Las sesion de la conexion
     */
	public Session getSession() {
		return session.getCurrentSession();
	}
	/**
     * Metodo que hace que la sesion de la clase sea la recibida
     * @param La sesion que sera nueva de la clase
     */
	public void setSession(SessionFactory session) {
		this.session = session;
	}
	/**
     * Metodo que nos da la lista de una consulta
     * @return Lista de objetos segun la consulta
     */
	@SuppressWarnings("unchecked")
	public List<Object> hacerConsulta(String consulta) {
		Session session = this.getSession();
		List <Object> listado=null;
		session.beginTransaction();
		listado=session.createQuery(consulta).list();
		session.getTransaction().commit();
		return listado;
	}
	/**
     * Metodo que nos modifica un objeto de una entidad en la base de datos
     * @param Object El objeto que se modificar� en la base de datos, reconoce que entidad es seg�n la instancia.
     */
	public void modificar(Object obj) {
		Session session = this.getSession();
		session.beginTransaction();
		session.merge(obj);
		session.getTransaction().commit();
	}
	/**
     * Metodo que nos agrega un objeto de una entidad en la base de datos
     * @param Object El objeto que se agregar� en la base de datos, reconoce que entidad es seg�n la instancia.
     */
	public void agregar(Object obj) {
		Session session = this.getSession();
		session.beginTransaction();
		session.save(obj);
		session.getTransaction().commit();
	}
	/**
     * Metodo que busca un objeto segun su id
     * @param Class<?> La clase que se buscara
     * @param Int el id que se buscara en caso de ser integer, primary key
     * @return El objeto que se encontr� con dichas especificaciones
     */
	public Object Buscar(Class<?> classX, int ID) {
		Session session = this.getSession();
		session.beginTransaction();
		Object objeto=session.get(classX, ID);
		session.getTransaction().commit();
		return objeto;
	}
	/**
     * Metodo que busca un objeto segun su id
     * @param Class<?> La clase que se buscara
     * @param String el id que se buscara en caso de ser varchar, primary key
     * @return El objeto que se encontr� con dichas especificaciones
     */
	public Object Buscar(Class<?> classX, String ID) {
		Session session = this.getSession();
		session.beginTransaction();
		Object objeto=session.get(classX, ID);
		session.getTransaction().commit();
		return objeto;
	}
	/**
     * Metodo que nos elimina un objeto de  una entidad en la base de datos
     * @param Object El objeto que se agregar� en la base de datos, reconoce que entidad es seg�n la instancia.
     */
	public void eliminar(Object obj) {
			Session session = getSession();
			session.beginTransaction();
			session.delete(obj);
			session.getTransaction().commit();
	}
	/**
     * Metodo que nos autentica un usuario
     * @param String Recibe el nombre de usuario que se 
     */
	public Usuario autenticarUsuario(String usr, String pass){
		Usuario usuario=null;
		try{
			usuario=(Usuario)this.hacerConsulta("From Usuario u where u.nombreUsuario='"+usr+"' and u.contrasenia='"+pass+"'").get(0);
		}catch(Exception e){
			
		}
		return usuario;
	}

	
	
}