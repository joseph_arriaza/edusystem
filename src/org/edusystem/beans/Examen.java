package org.edusystem.beans;

public class Examen {

	private Integer idExamen;
	private Tema tema;
	private String nombreExamen;
	public Integer getIdExamen() {
		return idExamen;
	}
	public void setIdExamen(Integer idExamen) {
		this.idExamen = idExamen;
	}
	public Tema getTema() {
		return tema;
	}
	public void setTema(Tema tema) {
		this.tema = tema;
	}
	public String getNombreExamen() {
		return nombreExamen;
	}
	public void setNombreExamen(String nombreExamen) {
		this.nombreExamen = nombreExamen;
	}
	public Examen() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Examen(Integer idExamen, Tema tema, String nombreExamen) {
		super();
		this.idExamen = idExamen;
		this.tema = tema;
		this.nombreExamen = nombreExamen;
	}
	
	
}
