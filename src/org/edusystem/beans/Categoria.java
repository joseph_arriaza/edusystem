package org.edusystem.beans;

public class Categoria {

	private Integer idCategoria;
	private String nombreCategoria;
	public Integer getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getNombreCategoria() {
		return nombreCategoria;
	}
	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}
	public Categoria() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Categoria(Integer idCategoria, String nombreCategoria) {
		super();
		this.idCategoria = idCategoria;
		this.nombreCategoria = nombreCategoria;
	}
	
	
	
}
