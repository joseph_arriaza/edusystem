package org.edusystem.beans;  
  
import java.io.Serializable;  
//Composite id  
public class GrupoAlumno implements Serializable{  
    private Grupo grupo;  
    private Alumno alumno;  
  
    public GrupoAlumno(Grupo grupo, Alumno alumno) {  
        this.grupo = grupo;  
        this.alumno = alumno;  
    }  
  
    public GrupoAlumno() {}  
  
    public Grupo getGrupo() {  
        return grupo;  
    }  
    public void setGrupo(Grupo type) {  
        this.grupo = type;  
    }  
    public Alumno getAlumno() {  
        return alumno;  
    }  
    public void setAlumno(Alumno customerId) {  
        this.alumno = customerId;  
    }  
    public boolean equals(Object other) {  
        if ( !(other instanceof GrupoAlumno) ) return false;  
        GrupoAlumno add = (GrupoAlumno) other;  
        return grupo.equals(add.grupo) && alumno.equals(add.alumno);  
    }  
    public int hashCode() {  
        return grupo.hashCode() + alumno.hashCode();  
    }  
  
    public String toString() {  
        return grupo + "#" + alumno;  
    }  
} 