package org.edusystem.beans;

import java.math.BigDecimal;

public class Pregunta {

	private Integer idPregunta;
	private String nombrePregunta,respuestaPregunta;
	private BigDecimal punteoPregunta;
	private Examen examen;
	public Integer getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getNombrePregunta() {
		return nombrePregunta;
	}
	public void setNombrePregunta(String nombrePregunta) {
		this.nombrePregunta = nombrePregunta;
	}
	public BigDecimal getPunteoPregunta() {
		return punteoPregunta;
	}
	public void setPunteoPregunta(BigDecimal punteoPregunta) {
		this.punteoPregunta = punteoPregunta;
	}
	public Examen getExamen() {
		return examen;
	}
	public void setExamen(Examen examen) {
		this.examen = examen;
	}
	
	public String getRespuestaPregunta() {
		return respuestaPregunta;
	}
	public void setRespuestaPregunta(String respuestaPregunta) {
		this.respuestaPregunta = respuestaPregunta;
	}
	public Pregunta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pregunta(Integer idPregunta, String nombrePregunta,
			String respuestaPregunta, BigDecimal punteoPregunta, Examen examen) {
		super();
		this.idPregunta = idPregunta;
		this.nombrePregunta = nombrePregunta;
		this.respuestaPregunta = respuestaPregunta;
		this.punteoPregunta = punteoPregunta;
		this.examen = examen;
	}
	
	
	
}
