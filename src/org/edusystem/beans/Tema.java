package org.edusystem.beans;

public class Tema {
	
	private Integer idTema;
	private String nombreTema;
	private Curso curso;
	public Integer getIdTema() {
		return idTema;
	}
	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}
	public String getNombreTema() {
		return nombreTema;
	}
	public void setNombreTema(String nombreTema) {
		this.nombreTema = nombreTema;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public Tema() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Tema(Integer idTema, String nombreTema, Curso curso) {
		super();
		this.idTema = idTema;
		this.nombreTema = nombreTema;
		this.curso = curso;
	}
	
	
}
