package org.edusystem.beans;


public class Grupo {
	
	private Integer idGrupo;
	private Profesor profesor;
	private String nombreGrupo,idAlumno;
	public Integer getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(Integer idGrupo) {
		this.idGrupo = idGrupo;
	}
	public Profesor getProfesor() {
		return profesor;
	}
	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	public String getNombreGrupo() {
		return nombreGrupo;
	}
	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}
	
	public String getIdAlumno() {
		return idAlumno;
	}
	public void setIdAlumno(String idAlumno) {
		this.idAlumno = idAlumno;
	}
	public Grupo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Grupo(Integer idGrupo, Profesor profesor, String nombreGrupo,
			String idAlumno) {
		super();
		this.idGrupo = idGrupo;
		this.profesor = profesor;
		this.nombreGrupo = nombreGrupo;
		this.idAlumno = idAlumno;
	}
	
	
	
	
}
