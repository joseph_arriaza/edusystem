package org.edusystem.beans;

import java.util.Date;

public class Curso {

	private Integer idCurso;
	private Profesor autor;
	private Categoria categoria;
	private Grupo grupo;
	private Date fechaPublicacion;
	private String nombreCurso, descripcion;
	public Integer getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(Integer idCurso) {
		this.idCurso = idCurso;
	}
	public Profesor getAutor() {
		return autor;
	}
	public void setAutor(Profesor autor) {
		this.autor = autor;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Curso() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public Curso(Integer idCurso, Profesor autor, Categoria categoria,
			Grupo grupo, String nombreCurso, String descripcion,
			Date fechaPublicacion) {
		super();
		this.idCurso = idCurso;
		this.autor = autor;
		this.categoria = categoria;
		this.grupo = grupo;
		this.nombreCurso = nombreCurso;
		this.descripcion = descripcion;
		this.fechaPublicacion = fechaPublicacion;
	}
	

	
	
}
