package org.edusystem.beans;

import java.util.Date;

public class Solicitud_Grupo {
	private Integer idSolicitud,estado;
	private Alumno alumno;
	private Grupo grupo;
	private Date fechaSolicitud;
	public Integer getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Alumno getAlumno() {
		return alumno;
	}
	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	public Solicitud_Grupo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Solicitud_Grupo(Integer idSolicitud, Integer estado, Alumno alumno,
			Grupo grupo, Date fechaSolicitud) {
		super();
		this.idSolicitud = idSolicitud;
		this.estado = estado;
		this.alumno = alumno;
		this.grupo = grupo;
		this.fechaSolicitud = fechaSolicitud;
	}
	
}
