package org.edusystem.beans;

public class Usuario {
	private Integer idUsuario;
	private Rol rol;
	private String nombreUsuario,contrasenia;
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Rol getRol() {
		return rol;
	}
	public void setRol(Rol rol) {
		this.rol = rol;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	
	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Usuario(Integer idUsuario, Rol rol, String nombreUsuario,
			String contrasenia) {
		super();
		this.idUsuario = idUsuario;
		this.rol = rol;
		this.nombreUsuario = nombreUsuario;
		this.contrasenia = contrasenia;
	}
	
	
}