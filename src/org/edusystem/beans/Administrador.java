package org.edusystem.beans;

public class Administrador extends RolUsuario{

	private Integer idAdministrador;
	private Usuario usuario;
	private String nombreAdministrador,apellidoAdministrador,correoAdministrador,telefonoAdministrador;
	public Integer getIdAdministrador() {
		return idAdministrador;
	}
	public void setIdAdministrador(Integer idAdministrador) {
		this.idAdministrador = idAdministrador;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNombreAdministrador() {
		return nombreAdministrador;
	}
	public void setNombreAdministrador(String nombreAdministrador) {
		this.nombreAdministrador = nombreAdministrador;
	}
	public String getApellidoAdministrador() {
		return apellidoAdministrador;
	}
	public void setApellidoAdministrador(String apellidoAdministrador) {
		this.apellidoAdministrador = apellidoAdministrador;
	}
	public String getCorreoAdministrador() {
		return correoAdministrador;
	}
	public void setCorreoAdministrador(String correoAdministrador) {
		this.correoAdministrador = correoAdministrador;
	}
	public String getTelefonoAdministrador() {
		return telefonoAdministrador;
	}
	public void setTelefonoAdministrador(String telefonoAdministrador) {
		this.telefonoAdministrador = telefonoAdministrador;
	}
	public Administrador() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Administrador(Integer idAdministrador, Usuario usuario,
			String nombreAdministrador, String apellidoAdministrador,
			String correoAdministrador, String telefonoAdministrador) {
		super(idAdministrador,usuario,nombreAdministrador,apellidoAdministrador,correoAdministrador,telefonoAdministrador);
		this.idAdministrador = idAdministrador;
		this.usuario = usuario;
		this.nombreAdministrador = nombreAdministrador;
		this.apellidoAdministrador = apellidoAdministrador;
		this.correoAdministrador = correoAdministrador;
		this.telefonoAdministrador = telefonoAdministrador;
	}
	
	
	
}
