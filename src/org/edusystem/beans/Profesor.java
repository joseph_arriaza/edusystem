package org.edusystem.beans;

public class Profesor extends RolUsuario{

	private Integer idProfesor;
	private Usuario usuario;
	private String nombreProfesor,apellidoProfesor,correoProfesor,fotoProfesor,direccionProfesor;
	public Integer getIdProfesor() {
		return idProfesor;
	}
	public void setIdProfesor(Integer idProfesor) {
		this.idProfesor = idProfesor;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNombreProfesor() {
		return nombreProfesor;
	}
	public void setNombreProfesor(String nombreProfesor) {
		this.nombreProfesor = nombreProfesor;
	}
	public String getApellidoProfesor() {
		return apellidoProfesor;
	}
	public void setApellidoProfesor(String apellidoProfesor) {
		this.apellidoProfesor = apellidoProfesor;
	}
	public String getCorreoProfesor() {
		return correoProfesor;
	}
	public void setCorreoProfesor(String correoProfesor) {
		this.correoProfesor = correoProfesor;
	}
	public String getFotoProfesor() {
		return fotoProfesor;
	}
	public void setFotoProfesor(String fotoProfesor) {
		this.fotoProfesor = fotoProfesor;
	}
	public String getDireccionProfesor() {
		return direccionProfesor;
	}
	public void setDireccionProfesor(String direccionProfesor) {
		this.direccionProfesor = direccionProfesor;
	}
	public Profesor() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Profesor(Integer idProfesor, Usuario usuario, String nombreProfesor,
			String apellidoProfesor, String correoProfesor,
			String fotoProfesor, String direccionProfesor) {
		super(idProfesor,usuario,nombreProfesor,apellidoProfesor,correoProfesor,fotoProfesor,direccionProfesor);
		this.idProfesor = idProfesor;
		this.usuario = usuario;
		this.nombreProfesor = nombreProfesor;
		this.apellidoProfesor = apellidoProfesor;
		this.correoProfesor = correoProfesor;
		this.fotoProfesor = fotoProfesor;
		this.direccionProfesor = direccionProfesor;
	}
	
	
	
}