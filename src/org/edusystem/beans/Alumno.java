package org.edusystem.beans;


public class Alumno extends RolUsuario{
	private Integer idAlumno;
	private Usuario usuario;
	private String nombreAlumno,apellidoAlumno,correoAlumno,fotoAlumno,direccionAlumno;
	public Integer getIdAlumno() {
		return idAlumno;
	}
	public void setIdAlumno(Integer idAlumno) {
		this.idAlumno = idAlumno;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public String getApellidoAlumno() {
		return apellidoAlumno;
	}
	public void setApellidoAlumno(String apellidoALumno) {
		this.apellidoAlumno = apellidoALumno;
	}
	public String getCorreoAlumno() {
		return correoAlumno;
	}
	public void setCorreoAlumno(String correoAlumno) {
		this.correoAlumno = correoAlumno;
	}
	public String getFotoAlumno() {
		return fotoAlumno;
	}
	public void setFotoAlumno(String fotoAlumno) {
		this.fotoAlumno = fotoAlumno;
	}
	public String getDireccionAlumno() {
		return direccionAlumno;
	}
	public void setDireccionAlumno(String direccionAlumno) {
		this.direccionAlumno = direccionAlumno;
	}
	public Alumno() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Alumno(Integer idAlumno, Usuario usuario, String nombreAlumno,
			String apellidoALumno, String correoAlumno, String fotoAlumno,
			String direccionAlumno) {
		super(idAlumno,usuario,nombreAlumno,apellidoALumno,correoAlumno,fotoAlumno,direccionAlumno);
		this.idAlumno = idAlumno;
		this.usuario = usuario;
		this.nombreAlumno = nombreAlumno;
		this.apellidoAlumno = apellidoALumno;
		this.correoAlumno = correoAlumno;
		this.fotoAlumno = fotoAlumno;
		this.direccionAlumno = direccionAlumno;
	}
	
	
}
