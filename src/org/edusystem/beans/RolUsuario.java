package org.edusystem.beans;

public abstract class RolUsuario{
	private Integer id;
	private Usuario user;
	private String nombre,apellido,correo,foto,direccion,telefono,username;
	
	public Integer getId() {
		return id;
	}

	public Usuario getUser() {
		return user;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public String getFoto() {
		return foto;
	}
	
	public String getDireccion() {
		return direccion;
	}

	public String getTelefono() {
		return telefono;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public RolUsuario(Integer id, Usuario user, String nombre, String apellido,
			String correo, String foto, String direccion) {
		super();
		this.id = id;
		this.user = user;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.foto = foto;
		this.direccion = direccion;
	}	

	public RolUsuario(Integer id, Usuario user, String nombre, String apellido,
			String correo, String telefono) {
		super();
		this.id = id;
		this.user = user;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.telefono = telefono;
	}

	public RolUsuario(){
		super();
	}
}
