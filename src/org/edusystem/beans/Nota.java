package org.edusystem.beans;

import java.math.BigDecimal;

public class Nota {
	private Integer idNota;
	private Examen examen;
	private Alumno alumno;
	private BigDecimal punteo;
	private Curso curso;
	public Integer getIdNota() {
		return idNota;
	}
	public void setIdNota(Integer idNota) {
		this.idNota = idNota;
	}
	public Examen getExamen() {
		return examen;
	}
	public void setExamen(Examen examen) {
		this.examen = examen;
	}
	public Alumno getAlumno() {
		return alumno;
	}
	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	public BigDecimal getPunteo() {
		return punteo;
	}
	public void setPunteo(BigDecimal punteo) {
		this.punteo = punteo;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public Nota() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Nota(Integer idNota, Examen examen, Alumno alumno, BigDecimal punteo,
			Curso curso) {
		super();
		this.idNota = idNota;
		this.examen = examen;
		this.alumno = alumno;
		this.punteo = punteo;
		this.curso = curso;
	}
	
	
}