package org.edusystem.servlet.image;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.edusystem.beans.Alumno;
import org.edusystem.beans.Profesor;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;

public class ServletUploadImage extends HttpServlet{

	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador =peticion.getRequestDispatcher("index.jsp");
		FileItemFactory file_factory = new DiskFileItemFactory();
		int idUFoto=0;
        ServletFileUpload servlet_up = new ServletFileUpload(file_factory);
        List items = null;
		try {
			items = servlet_up.parseRequest(peticion);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//if(peticion.getParameter("tipo").equals("producto")){
			despachador=peticion.getRequestDispatcher("index.jsp");
			for(int i=0;i<items.size();i++){
	            FileItem item = (FileItem) items.get(i);
	            System.out.println(item.getFieldName());
	            if(item.getFieldName().equals("idUsuario")){
	            	System.out.println(item.getFieldName() +" "+item.getString());
	            	idUFoto=Integer.parseInt(item.getString());
	            	System.out.println(idUFoto);
				}else if (! item.isFormField()){
	            	String rutaTemp=this.getServletContext().getRealPath("");
	            	rutaTemp=rutaTemp.replace('\\','/');
	    			System.out.println(rutaTemp);
	    			String rutaSplit[]=rutaTemp.split("/");
	    			String nuevaRuta="";
	    			System.out.println(rutaTemp);
	    			for(String ruta:rutaSplit){
	    				if(ruta.equalsIgnoreCase(".metadata")||ruta.equalsIgnoreCase(".plugins")||ruta.equalsIgnoreCase("org.eclipse.wst.server.core")||ruta.equalsIgnoreCase("tmp0")||ruta.equalsIgnoreCase("wtpwebapps")){
	    					
	    				}else{
	    					System.out.println(nuevaRuta);
	    					if(nuevaRuta.trim().equals("")){
	    						nuevaRuta=ruta;
	    					}else{
	    						if(!nuevaRuta.contains(item.getString())){
	    							nuevaRuta=nuevaRuta+"/"+ruta;
	    						}
	    					}
	    				}
	    			}
	    			nuevaRuta=rutaTemp+"/images/usuario/";
	    			System.out.println(nuevaRuta);
	            	File archivo_server = new File(nuevaRuta+item.getName());
					try {
						despachador=peticion.getRequestDispatcher("include/all.jsp");
						item.write(archivo_server);
						System.out.println(idUFoto);
						Usuario u=(Usuario)Conexion.getInstancia().Buscar(Usuario.class, idUFoto);
						System.out.println(u.getNombreUsuario());
						if(u.getRol().getIdRol()==2){
							Alumno al=(Alumno)peticion.getSession().getAttribute("rolUsuario");
							al.setFotoAlumno(item.getName());
							peticion.getSession().setAttribute("rolUsuario", al);
							Conexion.getInstancia().modificar(al);
						}else if(u.getRol().getIdRol()==3){
							Profesor p=(Profesor)peticion.getSession().getAttribute("rolUsuario");
							p.setFotoProfesor(item.getName());
							peticion.getSession().setAttribute("rolUsuario", p);
							Conexion.getInstancia().modificar(p);
						}
						peticion.setAttribute("estado","Imagen actualizada correctamente! Los cambios se realizar�n al iniciar sesi�n de nuevo!");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						peticion.setAttribute("estado","Error al subir imagen!");
						e.printStackTrace();
					}
	            }
	        }
		//}
		
		despachador.forward(peticion,respuesta);
	}
	
}
