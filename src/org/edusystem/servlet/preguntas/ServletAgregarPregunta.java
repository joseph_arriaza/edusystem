package org.edusystem.servlet.preguntas;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Pregunta;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;

/**
 * Servlet para agregar Pregunta
 */
public class ServletAgregarPregunta extends HttpServlet {

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	/**
     * Metodo que verifica que el punteo del examen sea 100 o no, y dice que punteo falta para alcanzarlo
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @return Devuelve la cantidad de puntos que hace falta para llegar a 100
     */
	public Double getPunteo(HttpServletRequest peticion){
		Double punteo=100.00;
		for(Object o:Conexion.getInstancia().hacerConsulta("From Pregunta where examen="+Integer.parseInt(peticion.getParameter("idExamen")))){
			Pregunta p=(Pregunta) o;
			punteo-=p.getPunteoPregunta().doubleValue();
		}
		return punteo-Double.parseDouble(peticion.getParameter("txtPunteoPregunta"));
	}

	/**
     * Metodo que agrega una pregunta al examen y verifica el estado para ver si redirecciona al formulario o agrega a la base de datos
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		try{
			if(peticion.getParameter("estadoAP").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListaPreguntas.do?idGrupo="+peticion.getParameter("idGrupo")+"&idCurso="+peticion.getParameter("idCurso")+"&idTema="+peticion.getParameter("idTema")+"&idExamen="+peticion.getParameter("idExamen"));

				Pregunta p=new Pregunta(0,
										peticion.getParameter("txtNombrePregunta"),
										peticion.getParameter("txtRespuestaPregunta"),
										new BigDecimal(peticion.getParameter("txtPunteoPregunta")),
										(Examen)Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
				peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
				peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
				peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
				peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
				Double punteoRestante=getPunteo(peticion);
				if(punteoRestante>=0.0){
					Double punteoFaltante=100-punteoRestante;
					System.out.println("PF: "+punteoFaltante);
					System.out.println("PT: "+punteoFaltante);
					Conexion.getInstancia().agregar(p);
					if(punteoFaltante==100){
						peticion.setAttribute("estado", "Pregunta agregada satisfactoriamente! Ha alcanzado los 100 puntos, su ex�men ha sido activado!");
					}else{
						peticion.setAttribute("estado", "Pregunta agregada satisfactoriamente! Recuerda que el examen se activar� a los alumnos hasta alcanzar los 100 puntos, y lleva "+punteoFaltante+"!");
					}
				}else{
					despachador=peticion.getRequestDispatcher("pregunta/add.jsp");
					Double pR=punteoRestante+Double.parseDouble(peticion.getParameter("txtPunteoPregunta"));
					peticion.setAttribute("estadoAP","La pregunta no puede tener ese punteo, solo faltan "+pR+" para alcanzar los 100 puntos del examen.");
				}
			}else{
				despachador=peticion.getRequestDispatcher("pregunta/add.jsp");
				peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
				peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
				peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
				peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
				peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
				peticion.setAttribute("estadoAP","El numero debe de contener solo un decimal, 10.5");
			}
		}catch(Exception e){
			e.printStackTrace();
			despachador=peticion.getRequestDispatcher("pregunta/add.jsp");
			peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
			peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
			peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
			peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
			peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
		}
		despachador.forward(peticion, respuesta);
	}

}
