package org.edusystem.servlet.preguntas;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Pregunta;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
/**
 * Servlet para eliminar un examen
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletEliminarPregunta extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que elimina un examen segun su id
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("examen/listarPregunta.jsp");
		Tema t=(Tema) Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema")));
		try{
			Pregunta p= new Pregunta();
			p.setIdPregunta(Integer.parseInt(peticion.getParameter("idPregunta")));
			Conexion.getInstancia().eliminar(p);
		}catch(Exception e){
			peticion.setAttribute("estado","Error al eliminar!");
		}
		peticion.setAttribute("estado", "Pregunta eliminada del examen!");
		peticion.setAttribute("listaPreguntas",Conexion.getInstancia().hacerConsulta("From Pregunta WHERE examen="+Integer.parseInt(peticion.getParameter("idExamen"))));
		peticion.setAttribute("grupoN", Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
		peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
		peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
		peticion.setAttribute("idG", peticion.getParameter("idGrupo"));
		despachador.forward(peticion, respuesta);
	}
}
