package org.edusystem.servlet.preguntas;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Pregunta;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;

/**
 * Servlet para agregar Pregunta
 */
public class ServletEditarPregunta extends HttpServlet {

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	/**
     * Metodo que verifica que el punteo del examen sea 100 o no, y dice que punteo falta para alcanzarlo
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @return Devuelve la cantidad de puntos que hace falta para llegar a 100
     */
	public Double getPunteo(HttpServletRequest peticion){
		Double punteo=100.00;
		for(Object o:Conexion.getInstancia().hacerConsulta("From Pregunta where examen="+Integer.parseInt(peticion.getParameter("idExamen")))){
			Pregunta p=(Pregunta) o;
			punteo-=p.getPunteoPregunta().doubleValue();
		}
		System.out.println(punteo-Double.parseDouble(peticion.getParameter("txtPunteoPregunta")));
		Double a=punteo-Double.parseDouble(peticion.getParameter("txtPunteoPregunta"));
		return a+Double.parseDouble(peticion.getParameter("txtPunteoAntiguo"));
	}

	/**
     * Metodo que edita una pregunta al examen y verifica el estado para ver si redirecciona al formulario o editar a la base de datos
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Pregunta p=(Pregunta)Conexion.getInstancia().Buscar(Pregunta.class, Integer.parseInt(peticion.getParameter("idPregunta")));
		try{
			if(peticion.getParameter("estadoEP").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListaPreguntas.do?idGrupo="+peticion.getParameter("idGrupo")+"&idCurso="+peticion.getParameter("idCurso")+"&idTema="+peticion.getParameter("idTema")+"&idExamen="+peticion.getParameter("idExamen"));
				peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
				peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
				peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
				peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
				Double punteoRestante=getPunteo(peticion);
				if(punteoRestante>=0.0){
					Double punteoFaltante=100-punteoRestante;
					System.out.println("PF: "+punteoFaltante);
					System.out.println("PT: "+punteoFaltante);
					p.setNombrePregunta(peticion.getParameter("txtNombrePregunta"));
					p.setRespuestaPregunta(peticion.getParameter("txtRespuestaPregunta"));
					p.setPunteoPregunta(new BigDecimal(Double.parseDouble(peticion.getParameter("txtPunteoPregunta"))));
					Conexion.getInstancia().modificar(p);
					if(punteoFaltante==100){
						peticion.setAttribute("estado", "Pregunta agregada satisfactoriamente! Ha alcanzado los 100 puntos, su ex�men ha sido activado!");
					}else{
						peticion.setAttribute("estado", "Pregunta agregada satisfactoriamente! Recuerda que el examen se activar� a los alumnos hasta alcanzar los 100 puntos, y lleva "+punteoFaltante+"!");
					}
				}else{
					despachador=peticion.getRequestDispatcher("pregunta/editar.jsp");
					Double pR=punteoRestante+Double.parseDouble(peticion.getParameter("txtPunteoPregunta"));
					peticion=getPeticion(peticion,p);
					peticion.setAttribute("estadoEP","La pregunta no puede tener ese punteo, solo faltan "+pR+" para alcanzar los 100 puntos del examen.");
				}
			}else{
				despachador=peticion.getRequestDispatcher("pregunta/editar.jsp");
				peticion=getPeticion(peticion,p);
				peticion.setAttribute("estadoEP","El numero debe de contener solo un decimal, 10.5");
			}
		}catch(Exception e){
			e.printStackTrace();
			despachador=peticion.getRequestDispatcher("pregunta/editar.jsp");
			peticion=getPeticion(peticion,p);
		}
		despachador.forward(peticion, respuesta);
	}
	
	/**
     * Metodo que da atributos a la peticion
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @return Retorna la petici�n enviada, pero con sus nuevos atributos
     */
	public HttpServletRequest getPeticion(HttpServletRequest peticion,Pregunta p){
		peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
		peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
		peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
		peticion.setAttribute("pregunta",p.getNombrePregunta());
		peticion.setAttribute("respuesta",p.getRespuestaPregunta());
		peticion.setAttribute("punteo",p.getPunteoPregunta());
		peticion.setAttribute("idPregunta",p.getIdPregunta());
		peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
		return peticion;
	}

}
