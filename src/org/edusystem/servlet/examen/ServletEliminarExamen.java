package org.edusystem.servlet.examen;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
import org.edusystem.utilidad.CascadeDelete;
/**
 * Servlet para eliminar un examen
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletEliminarExamen extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que elimina un examen segun su id
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("theme/listarExamen.jsp");
		Tema t=(Tema) Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema")));
		try{
			Examen e= new Examen();
			System.out.println("Instancia");
			e.setIdExamen(Integer.parseInt(peticion.getParameter("idExamen")));
			System.out.println("Set id");
			Conexion.getInstancia().eliminar(e);
			System.out.println("Eliminaod");
		}catch(Exception e){
			if(CascadeDelete.instancia.delExamen(t)){
				peticion.setAttribute("estado", "Examen eliminado correctamente!");
			}else{
				peticion.setAttribute("estado", "Error al eliminar!");
			}
		}
		peticion.setAttribute("estado", "Examen eliminado del curso!");
		peticion.setAttribute("listaExamen",Conexion.getInstancia().hacerConsulta("From Examen WHERE idTema="+Integer.parseInt(peticion.getParameter("idTema"))));
		peticion.setAttribute("grupoN", Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
		peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
		peticion.setAttribute("idG", peticion.getParameter("idGrupo"));
		despachador.forward(peticion, respuesta);
	}
}
