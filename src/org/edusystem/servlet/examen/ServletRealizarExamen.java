package org.edusystem.servlet.examen;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Nota;
import org.edusystem.beans.Pregunta;
import org.edusystem.db.Conexion;

/**
 * Servlet para realizar el examen
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletRealizarExamen extends HttpServlet{


	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}

	/**
     * Metodo que nos da las variables para realizar el examen y examina si se redirecciona al formulario o si el examen ya se realiz�
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		try{
			if(peticion.getParameter("realizado").equals("true")){
				despachador=peticion.getRequestDispatcher("index.jsp");
				Double nota=0.0;
				int campos=Integer.parseInt(peticion.getParameter("campos"));
				int idExamen=Integer.parseInt(peticion.getParameter("txtIdExamen"));
				int idCurso=Integer.parseInt(peticion.getParameter("txtIdCurso"));
				int campoI=campos;
				for(Object o:Conexion.getInstancia().hacerConsulta("From Pregunta where examen="+idExamen)){
					Pregunta p=(Pregunta) o;
					if(peticion.getParameter(""+campoI).toUpperCase().contains(p.getRespuestaPregunta().toUpperCase())){
						nota+=p.getPunteoPregunta().doubleValue();
					}
					campoI-=1;
				}
				Nota n=new Nota(0,
								(Examen)Conexion.getInstancia().Buscar(Examen.class, idExamen),
								(Alumno)peticion.getSession().getAttribute("rolUsuario"),
								new BigDecimal(nota),
								(Curso)Conexion.getInstancia().Buscar(Curso.class, idCurso));
				Conexion.getInstancia().agregar(n);
				peticion.setAttribute("estado","Examen realizado su nota es "+nota);
			}
		}catch(Exception exc){
			try{
				Alumno al=(Alumno)peticion.getSession().getAttribute("rolUsuario");
				int idExamen=Integer.parseInt(peticion.getParameter("idExamen"));
				if(!getEstadoExamenAlumno(al.getIdAlumno(),idExamen)){
					despachador=peticion.getRequestDispatcher("examen/realizarExamen.jsp");
					peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class,idExamen));
					peticion.setAttribute("idCurso",peticion.getParameter("idCurso"));
					List <Object> lista=Conexion.getInstancia().hacerConsulta("From Pregunta where examen="+idExamen);
					peticion.setAttribute("listaPreguntas", lista);
					peticion.setAttribute("cantidadPreguntas",lista.size());
				}else{
					despachador=peticion.getRequestDispatcher("index.jsp");
					peticion.setAttribute("estado","Ya haz realizado este examen, tu nota fue: "+getNota(al.getIdAlumno(),idExamen));
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		despachador.forward(peticion, respuesta);
	}
	
	/**
     * Metodo que verifica que el alumno no haya hecho aun el examen
     * @param idAlumno es el id del alumno que se verificar� si ya hizo el examen o no
     * @param idExamen examen a verificar si ya se hizo o no
     */
	public boolean getEstadoExamenAlumno(int idAlumno,int idExamen){
		for(Object o:Conexion.getInstancia().hacerConsulta("From Nota where idAlumno="+idAlumno+" and idExamen="+idExamen)){
			return true;
		}
		return false;
	}
	
	/**
     * Metodo que nos dice la nota de dicho examen
     * @param idAlumno es el id del alumno que hizo el examen
     * @param idExamen examen que se realiazo el alumno
     */
	public Double getNota(int idAlumno,int idExamen){
		Double nota=0.0;
		for(Object o:Conexion.getInstancia().hacerConsulta("From Nota where idAlumno="+idAlumno+" and idExamen="+idExamen)){
			nota=((Nota)o).getPunteo().doubleValue();
		}
		return nota;
	}
}