package org.edusystem.servlet.examen;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Pregunta;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;

/**
 * Servlet para lista los temas
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletListarExamen extends HttpServlet{


	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}

	/**
     * Metodo que lista los examenes segun el tema
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("theme/listarExamen.jsp");
		peticion.setAttribute("grupoN", Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
		peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
		peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
		if(peticion.getSession().getAttribute("buscarExamenPts").equals("false")){
			peticion.setAttribute("listaExamen",Conexion.getInstancia().hacerConsulta("From Examen where idTema="+Integer.parseInt(peticion.getParameter("idTema"))));
		}else{
			peticion.setAttribute("listaExamen",getExamenList(Integer.parseInt(peticion.getParameter("idTema"))));
		}
		despachador.forward(peticion, respuesta);
	}
	
	/**
     * Metodo que lista los examenes segun el tema que tienen los 100 puntos para llegar al maximo
     * @param idTema es el id del tema el cual se buscara su examen para ver si es mayor de 100 puntos, si lo es, lo devuelve para que se pueda realizar
     * @return Lista de los examenes de dicho tema que cumple a los 100 puntos
     */
	public ArrayList<Examen> getExamenList(int idTema){
		ArrayList<Examen> listaExamen = new ArrayList<Examen>();
		for(Object examenTema:Conexion.getInstancia().hacerConsulta("From Examen where idTema="+idTema)){
			Double pts=0.0;
			Examen e=(Examen) examenTema;
			for(Object examenPunteo:Conexion.getInstancia().hacerConsulta("From Pregunta where examen="+e.getIdExamen())){
				Pregunta p=(Pregunta)examenPunteo;
				pts+=p.getPunteoPregunta().doubleValue();
			}
			if(pts==100){
				listaExamen.add(e);
			}
		}
		return listaExamen;
	}
}
