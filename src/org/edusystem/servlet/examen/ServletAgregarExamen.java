package org.edusystem.servlet.examen;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Categoria;
import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
/**
 * Servlet para agregar un examen
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletAgregarExamen extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que agrega un examen para dicho tema, y se verifica el estado para ver si redirecciona o agrega
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=null;
		try{
			if(peticion.getParameter("estadoAE").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListarExamen.do?idGrupo="+peticion.getParameter("idGrupo")+"&idCurso="+peticion.getParameter("idCurso")+"&idTema="+peticion.getParameter("idTema"));
				Examen t=new Examen(0,(Tema)Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))),peticion.getParameter("txtNombreExamen"));
				peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
				peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
				peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
				Conexion.getInstancia().agregar(t);
				peticion.setAttribute("estado", "Examen agregado satisfactoriamente!");
			}else{
				despachador=peticion.getRequestDispatcher("examen/agregar.jsp");
				peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
				peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
				peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
				//peticion.setAttribute("examenN",Conexion.getInstancia().Buscar(Examen.class, Integer.parseInt(peticion.getParameter("idExamen"))));
				peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
			}
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("examen/agregar.jsp");
			peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
			peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
			peticion.setAttribute("temaN",Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema"))));
			peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
		}
		despachador.forward(peticion, respuesta);
	}
	
}
