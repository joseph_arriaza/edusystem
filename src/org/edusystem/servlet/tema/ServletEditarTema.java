package org.edusystem.servlet.tema;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
/**
 * Servlet para editar un tema
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEditarTema extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que edita un tema con su estadoEstadoTema se verifica si esta a editarse, o a llamar el formulario
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=null;
		Tema t= (Tema) Conexion.getInstancia().Buscar(Tema.class, Integer.parseInt(peticion.getParameter("idTema")));
		try{
			if(peticion.getParameter("estadoET").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListarTema.do?idGrupo="+peticion.getParameter("idGrupo")+"&idCurso="+peticion.getParameter("idCurso"));
				t.setNombreTema(peticion.getParameter("txtNombreTema"));
				Conexion.getInstancia().modificar(t);
				peticion.setAttribute("estado","Tema modificado!");
			}else{
				despachador=peticion.getRequestDispatcher("theme/editar.jsp");
				peticion.setAttribute("idG", peticion.getParameter("idGrupo"));
				peticion.setAttribute("idCurso", peticion.getParameter("idCurso"));
				peticion.setAttribute("idTema", t.getIdTema());
				peticion.setAttribute("nombreT",t.getNombreTema());
			}
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("theme/editar.jsp");
			peticion.setAttribute("idG", peticion.getParameter("idGrupo"));
			peticion.setAttribute("idCurso", peticion.getParameter("idCurso"));
			peticion.setAttribute("idTema", t.getIdTema());
			peticion.setAttribute("nombreT",t.getNombreTema());
		}
		despachador.forward(peticion, respuesta);
	}
	
}
