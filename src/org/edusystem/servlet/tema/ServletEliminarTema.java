package org.edusystem.servlet.tema;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
import org.edusystem.utilidad.CascadeDelete;

/**
 * Servlet para eliminar un tema
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEliminarTema extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}/**
     * Metodo que elimina el tema de la base de datos segun su id
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("course/listarTema.jsp");
		Curso c=(Curso) Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso")));
		try{
			Tema t = new Tema();
			t.setIdTema(Integer.parseInt(peticion.getParameter("idTema")));
			Conexion.getInstancia().eliminar(t);
		}catch(Exception e){
			if(CascadeDelete.instancia.delTema(c)){
				peticion.setAttribute("estado", "Tema eliminado correctamente!");
			}else{
				peticion.setAttribute("estado", "Error al eliminar!");
			}
		}
		peticion.setAttribute("estado", "Tema eliminado del curso!");
		peticion.setAttribute("listaTema",Conexion.getInstancia().hacerConsulta("From Tema WHERE curso="+c.getIdCurso()));
		peticion.setAttribute("grupoN", Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("cursoN",Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso"))));
		peticion.setAttribute("idG", c.getGrupo().getIdGrupo());
		despachador.forward(peticion, respuesta);
	}
}
