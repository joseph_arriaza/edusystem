package org.edusystem.servlet.tema;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
/**
 * Servlet para agregar un tema
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletAgregarTema extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que verifica el estadoAT(estadoAgregarTema) si es true, es porque el tema ser� agregado en ese instante, en otro caso, redirecciona la clase .JSP que tiene el formulario para agregarlo, si entra en true, agrega el tema para dicho curso que se obtiene su id por medio de la peticion
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=null;
		try{
			if(peticion.getParameter("estadoAT").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListarTema.do?idGrupo="+peticion.getParameter("idGrupo")+"&idCurso="+peticion.getParameter("idCurso"));
				Curso c = (Curso) Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso")));
				Tema t = new Tema(0,peticion.getParameter("txtNombreTema"),c);
				Conexion.getInstancia().agregar(t);
				peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
				peticion.setAttribute("estado","Tema agregado!");
			}else{
				despachador=peticion.getRequestDispatcher("theme/agregar.jsp");
				peticion.setAttribute("idCurso",peticion.getParameter("idCurso"));
				peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
			}
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("theme/agregar.jsp");
			peticion.setAttribute("idCurso",peticion.getParameter("idCurso"));
			peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
		}
		despachador.forward(peticion, respuesta);
	}
}
