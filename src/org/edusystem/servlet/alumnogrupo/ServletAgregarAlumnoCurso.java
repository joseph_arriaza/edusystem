package org.edusystem.servlet.alumnogrupo;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.db.Conexion;
/**
 * Servlet para agregar alumnos a un curso
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletAgregarAlumnoCurso extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que agrega el alumno al curso
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/listaAlumno.jsp");
		Grupo g=(Grupo) Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
		int estado=0;
		try{
			String idAls[]=g.getIdAlumno().split(" ");
			for(String idA:idAls){
				if(!idA.equals(peticion.getParameter("idAlumno"))){
					estado=1;
				}
			}
		}catch(Exception exc){
			estado=2;
		}
		System.out.println(estado);
		try{
			System.out.println(estado);
			if(estado==1){
				if(g.getIdAlumno().trim().equals("")){
					g.setIdAlumno(peticion.getParameter("idAlumno"));
				}else{
					g.setIdAlumno(g.getIdAlumno()+" "+peticion.getParameter("idAlumno"));
				}
			}else if(estado==2){
				g.setIdAlumno(peticion.getParameter("idAlumno"));
			}else{
				peticion.setAttribute("estado","Este alumno ya est� en el grupo!");
			}
			System.out.println(estado);
		}catch(Exception ex){
			System.out.println(estado);
			if(estado==2){
				g.setIdAlumno(peticion.getParameter("idAlumno"));
				System.out.println(peticion.getParameter("idAlumno"));
				System.out.println(g.getIdAlumno());
			}else{
				peticion.setAttribute("estado","Este alumno ya est� en el grupo!");
			}
		}
		Conexion.getInstancia().modificar(g);
		peticion.setAttribute("estado", "Usuario agregado al grupo!");
		peticion.setAttribute("grupo",g);
		ArrayList<Alumno> listaA=new ArrayList<Alumno>();
		try{
			String idAl[]=g.getIdAlumno().split(" ");
			for(String idA:idAl){
				listaA.add((Alumno)Conexion.getInstancia().Buscar(Alumno.class,Integer.parseInt(idA)));
			}
			peticion.setAttribute("listaAG",listaA);
		}catch(Exception e){
			
		}
		despachador.forward(peticion,respuesta);
	}
	
}
