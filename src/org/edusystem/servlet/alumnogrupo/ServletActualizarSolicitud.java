package org.edusystem.servlet.alumnogrupo;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Solicitud_Grupo;
import org.edusystem.db.Conexion;


/**
 * Servlet para actualizar la solicitud de un alumno al grupo, aceptarlo o eliminarlo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletActualizarSolicitud extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que envia la solicitud a dicho grupo
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		int idSolicitud=Integer.parseInt(peticion.getParameter("idSolicitud"));
		Solicitud_Grupo s_g=(Solicitud_Grupo) Conexion.getInstancia().Buscar(Solicitud_Grupo.class, idSolicitud);
		if(peticion.getParameter("estadoS").equals("true")){
			s_g.setEstado(2);
			Grupo g=(Grupo)Conexion.getInstancia().Buscar(Grupo.class, s_g.getGrupo().getIdGrupo());
			try{
				if(g.getIdAlumno().trim().equals("")){
					g.setIdAlumno(""+s_g.getAlumno().getIdAlumno());
				}else{
					g.setIdAlumno(g.getIdAlumno()+" "+s_g.getAlumno().getIdAlumno());
				}
			}catch(Exception ex){
				g.setIdAlumno(""+s_g.getAlumno().getIdAlumno());
			}
			Conexion.getInstancia().modificar(g);
			peticion.setAttribute("estado", "Se ha agregado el alumno al grupo");
		}else if(peticion.getParameter("estadoS").equals("false")){
			s_g.setEstado(1);
			peticion.setAttribute("estado", "Se ha cancelado la solicitud");
		}
		Conexion.getInstancia().modificar(s_g);
		despachador.forward(peticion, respuesta);
	}
}
