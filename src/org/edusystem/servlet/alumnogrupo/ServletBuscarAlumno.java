package org.edusystem.servlet.alumnogrupo;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;
/**
 * Servlet para buscar un alumno segun su username
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletBuscarAlumno extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que busca el alumno segun su username
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/listaAlumno.jsp");
		Grupo g=(Grupo) Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
		try{
			ArrayList<Alumno> listaAl=new ArrayList<Alumno>();
			for(Object o:Conexion.getInstancia().hacerConsulta("From Usuario where nombreUsuario like '%"+peticion.getParameter("txtUsernameAl")+"%' and idRol=2")){
				Usuario u=(Usuario) o;
				for(Object oA:Conexion.getInstancia().hacerConsulta("From Alumno where idUsuario="+u.getIdUsuario())){
					Alumno a=(Alumno) oA;
					listaAl.add(a);
				}
			}
			peticion.setAttribute("listaAl",listaAl);
		}catch(Exception e){
			
		}
		peticion.setAttribute("grupo",g);
		despachador.forward(peticion,respuesta);
	}
	
}
