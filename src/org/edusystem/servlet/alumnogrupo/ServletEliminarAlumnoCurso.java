package org.edusystem.servlet.alumnogrupo;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.db.Conexion;
/**
 * Servlet para eliminar un alumno del curso
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEliminarAlumnoCurso extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que elimina el alumno del curso
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/listaAlumno.jsp");
		Grupo g=(Grupo) Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
		
		String idAlDel[]=g.getIdAlumno().split(" ");
		String idAlNew="";
		for(String aDel:idAlDel){
			if(!aDel.equals(peticion.getParameter("idAlumno"))){
				if(idAlNew.trim().equals("")){
					idAlNew=aDel;
				}else{
					idAlNew=idAlNew+" "+aDel;
				}
			}
		}
		g.setIdAlumno(idAlNew);
		Conexion.getInstancia().modificar(g);
		peticion.setAttribute("estado", "Usuario eliminado del grupo!");
		peticion.setAttribute("grupo",g);
		ArrayList<Alumno> listaA=new ArrayList<Alumno>();
		try{
			String idAl[]=g.getIdAlumno().split(" ");
			for(String idA:idAl){
				listaA.add((Alumno)Conexion.getInstancia().Buscar(Alumno.class,Integer.parseInt(idA)));
			}
			peticion.setAttribute("listaAG",listaA);
		}catch(Exception e){
			
		}
		despachador.forward(peticion,respuesta);
	}
	
}
