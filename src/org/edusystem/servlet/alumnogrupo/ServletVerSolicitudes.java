package org.edusystem.servlet.alumnogrupo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Grupo;
import org.edusystem.db.Conexion;


/**
 * Servlet para ver las solicitudes en el grupo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletVerSolicitudes extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que ve la lista de solicitudes de dicho grupo
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/solicitudes.jsp");
		try{
			System.out.println(peticion.getParameter("admin"));
			peticion.setAttribute("listaSolicitud",Conexion.getInstancia().hacerConsulta("From Solicitud_Grupo where estado<2 and idGrupo="+Integer.parseInt(peticion.getParameter("idGrupo"))));
			System.out.println(peticion.getParameter("idGrupo"));
			peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		}catch(Exception ex){
			peticion.setAttribute("listaSolicitud",Conexion.getInstancia().hacerConsulta("From Solicitud_Grupo where estado=0 and idGrupo="+Integer.parseInt(peticion.getParameter("idGrupo"))));
			peticion.setAttribute("grupoN",Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		}
		despachador.forward(peticion, respuesta);
	}
}
