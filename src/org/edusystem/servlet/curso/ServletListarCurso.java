package org.edusystem.servlet.curso;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Grupo;
import org.edusystem.beans.Curso;
import org.edusystem.beans.Profesor;
import org.edusystem.db.Conexion;
/**
 * Servlet para lista los cursos
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletListarCurso extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}/**
     * Metodo que lista los cursos segun el grupo
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/listarCurso.jsp");
		peticion.setAttribute("listaCursos",Conexion.getInstancia().hacerConsulta("From Curso WHERE grupo="+Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("grupoN", Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))));
		peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
		despachador.forward(peticion, respuesta);
	}
}
