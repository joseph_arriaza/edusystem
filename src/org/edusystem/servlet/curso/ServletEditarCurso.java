package org.edusystem.servlet.curso;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Categoria;
import org.edusystem.beans.Curso;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.db.Conexion;
/**
 * Servlet para editar los cursos
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEditarCurso extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}

	/**
     * Metodo que edita un curso segun su id, y verifica el estado para ver si edita directamente o redirecciona al formulario
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=null;
		Curso c= (Curso) Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso")));
		try{
			if(peticion.getParameter("estadoEC").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListarCurso.do?idGrupo="+peticion.getParameter("idGrupo"));
				Categoria ca= (Categoria)Conexion.getInstancia().Buscar(Categoria.class, Integer.parseInt(peticion.getParameter("txtCategoria")));
				c.setIdCurso(Integer.parseInt(peticion.getParameter("idCurso")));
				c.setNombreCurso(peticion.getParameter("txtNombreCurso"));
				c.setDescripcion(peticion.getParameter("txtDescripcion"));
				Conexion.getInstancia().modificar(c);
			}else{
				despachador=peticion.getRequestDispatcher("course/editar.jsp");
				peticion.setAttribute("idCurso",peticion.getParameter("idCurso"));
				peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
				peticion.setAttribute("cat",Conexion.getInstancia().hacerConsulta("From Categoria"));
			}
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("course/editar.jsp");
			peticion.setAttribute("nombreC",c.getNombreCurso());
			peticion.setAttribute("descriC",c.getDescripcion());
			peticion.setAttribute("idG",peticion.getParameter("idGrupo"));
			peticion.setAttribute("idCurso",peticion.getParameter("idCurso"));
			peticion.setAttribute("cat",Conexion.getInstancia().hacerConsulta("From Categoria"));
		}
		despachador.forward(peticion, respuesta);
	}
	
}
