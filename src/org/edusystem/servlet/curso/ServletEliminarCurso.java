package org.edusystem.servlet.curso;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Curso;
import org.edusystem.db.Conexion;
import org.edusystem.utilidad.CascadeDelete;
/**
 * Servlet para eliminar los cursos
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletEliminarCurso extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que elimina un curso segun su id
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/listarCurso.jsp");
		Curso c=(Curso) Conexion.getInstancia().Buscar(Curso.class, Integer.parseInt(peticion.getParameter("idCurso")));
		try{
			Conexion.getInstancia().eliminar(c);
		}catch(Exception e){
			if(CascadeDelete.instancia.delCurso(Integer.parseInt(peticion.getParameter("idGrupo")))){
				peticion.setAttribute("estado", "Curso eliminado correctamente!");
			}else{
				peticion.setAttribute("estado", "Error al eliminar!");
			}
		}
		peticion.setAttribute("estado", "Curso eliminado del grupo!");
		peticion.setAttribute("listaCursos",Conexion.getInstancia().hacerConsulta("From Curso WHERE grupo="+c.getGrupo().getIdGrupo()));
		peticion.setAttribute("idG", c.getGrupo().getIdGrupo());
		despachador.forward(peticion, respuesta);
	}
}
