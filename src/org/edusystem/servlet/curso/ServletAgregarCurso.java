package org.edusystem.servlet.curso;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Categoria;
import org.edusystem.beans.Curso;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.db.Conexion;
/**
 * Servlet para agregar los cursos
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletAgregarCurso extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que agrega un curso segun su id de grupo, y verifica el estado para ver si agrega directamente o redirecciona al formulario
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=null;
		try{
			if(peticion.getParameter("estadoAC").equals("true")){
				despachador=peticion.getRequestDispatcher("ServletListarCurso.do?idGrupo="+peticion.getParameter("idGrupo"));
				Profesor p = (Profesor) peticion.getSession().getAttribute("rolUsuario");
				Categoria ca= (Categoria)Conexion.getInstancia().Buscar(Categoria.class, Integer.parseInt(peticion.getParameter("txtCategoria")));
				Grupo g = (Grupo) Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
				Curso c = new Curso(0,p,ca,g,peticion.getParameter("txtNombreCurso"),peticion.getParameter("txtDescripcion"),new Date());
				Conexion.getInstancia().agregar(c);
			}else{
				despachador=peticion.getRequestDispatcher("course/agregar.jsp");
				peticion.setAttribute("idGrupo",peticion.getParameter("idGrupo"));
				peticion.setAttribute("cat",Conexion.getInstancia().hacerConsulta("From Categoria"));
			}
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("course/agregar.jsp");
			peticion.setAttribute("idGrupo",peticion.getParameter("idGrupo"));
			peticion.setAttribute("cat",Conexion.getInstancia().hacerConsulta("From Categoria"));
		}
		despachador.forward(peticion, respuesta);
	}
	
}
