package org.edusystem.servlet.grupo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.db.Conexion;

/**
 * Servlet para editar un grupo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEditGroup extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que edita el grupo seleccionado segun el profesor iniciado, y revisa su estado para ver si ya se esta a agregar o debe redireccionar al formulario
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/edit.jsp");
		Profesor prof=((Profesor)peticion.getSession().getAttribute("rolUsuario"));
		try{
			if(peticion.getParameter("estadoAdd").equals("true")){
				despachador=peticion.getRequestDispatcher("usuario/dashboard.jsp");
				Grupo g=(Grupo)Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
				g.setNombreGrupo(peticion.getParameter("txtNombreGrupo"));
				Conexion.getInstancia().modificar(g);
				peticion.setAttribute("estado", "Grupo editado correctamente!");
				peticion.getSession().setAttribute("listaGrupo", Conexion.getInstancia().hacerConsulta("From Grupo WHERE idProfesor="+prof.getIdProfesor()));
			}else{
				Grupo gE=(Grupo)Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
				peticion.setAttribute("idG",gE.getIdGrupo());
				peticion.setAttribute("nG",gE.getNombreGrupo());
			}
		}catch(Exception e){
			Grupo gE=(Grupo)Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo")));
			peticion.setAttribute("idG",gE.getIdGrupo());
			peticion.setAttribute("nG",gE.getNombreGrupo());
		}
		despachador.forward(peticion, respuesta);
	}
}
