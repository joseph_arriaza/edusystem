package org.edusystem.servlet.grupo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.db.Conexion;
import org.edusystem.utilidad.CascadeDelete;

/**
 * Servlet para eliminar un grupo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletRemoveGroup extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que elimina un grupo segun su id
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		Profesor p=((Profesor)peticion.getSession().getAttribute("rolUsuario"));
		Grupo g=new Grupo();
		g.setIdGrupo(Integer.parseInt(peticion.getParameter("idGrupo")));
		try{
			if(CascadeDelete.instancia.delAll(Integer.parseInt(peticion.getParameter("idGrupo")))){
				peticion.setAttribute("estado", "Grupo eliminado correctamente!");
			}else{
				peticion.setAttribute("estado", "Error al eliminar!");
			}
		}catch(Exception e){
			peticion.setAttribute("estado", "Error al eliminar!");
			//Conexion.getInstancia().ejecutarSentencia("Delete From Grupo WHERE idGrupo="+Integer.parseInt(peticion.getParameter("idGrupo")),"idGrupo",Integer.parseInt(peticion.getParameter("idGrupo")));
		}
		//DELETE FROM tabla1, tabla2, tabla2, tabla4 
		//WHERE tabla1.ID = '[ID A ELIMINAR]'
		//AND tabla2.ID = tabla1.ID
		//AND tabla3.ID = tabla2.ID
		//AND tabla4.ID2 = tabla3.ID2
		peticion.getSession().setAttribute("listaGrupo", Conexion.getInstancia().hacerConsulta("From Grupo WHERE idProfesor="+p.getIdProfesor()));
		despachador.forward(peticion, respuesta);
	}
}
