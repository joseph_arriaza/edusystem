package org.edusystem.servlet.grupo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.db.Conexion;


/**
 * Servlet para agregar un grupo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletAddGroup extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que agrega un grupo a la lista de grupos del profesor que ha iniciado
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("usuario/dashboard.jsp");
		Profesor prof=((Profesor)peticion.getSession().getAttribute("rolUsuario"));
		Grupo g=new Grupo(0,prof,peticion.getParameter("txtNombreGrupo"),"");
		Conexion.getInstancia().agregar(g);
		peticion.setAttribute("estado", "Grupo agregado correctamente!");
		peticion.getSession().setAttribute("listaGrupo", Conexion.getInstancia().hacerConsulta("From Grupo WHERE idProfesor="+prof.getIdProfesor()));
		despachador.forward(peticion, respuesta);
	}
}
