package org.edusystem.servlet.grupo;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.db.Conexion;


/**
 * Servlet para buscar un grupo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletBuscarGrupo extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que lista los grupos encontrados
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("group/buscar.jsp");
		peticion.setAttribute("listaGruposBusqueda", getListaGrupos(peticion.getParameter("txtNombreGrupo").toUpperCase(), peticion));
		despachador.forward(peticion, respuesta);
	}
	
	public ArrayList<Grupo> getListaGrupos(String nombreGrupo,HttpServletRequest peticion){
		ArrayList<Grupo> listaGrupos=new ArrayList<Grupo>();
		for(Object o:Conexion.getInstancia().hacerConsulta("From Grupo where UPPER(nombreGrupo) like '%"+nombreGrupo+"%'")){
			Grupo g=(Grupo) o;
			try{
				Alumno alM=(Alumno)peticion.getSession().getAttribute("rolUsuario");
				String idAl[]=g.getIdAlumno().split(" ");
				System.out.println(g.getIdAlumno());
				if(getEstado(idAl,alM)){
					listaGrupos.add(g);
				}
			}catch(Exception ex){
				listaGrupos.add(g);
			}
		}
		return listaGrupos;
	}
	
	public boolean getEstado(String[] idAl,Alumno alM){
		for(String id:idAl){
			System.out.println(id);
			if(!id.equals(""+alM.getIdAlumno())){
				System.out.println(id);
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
}
