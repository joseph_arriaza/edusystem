package org.edusystem.servlet.grupo;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Solicitud_Grupo;
import org.edusystem.db.Conexion;


/**
 * Servlet para enviar solicitud a un profesor para entrar a su grupo
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletSolicitudGrupo extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que envia la solicitud a dicho grupo
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		Solicitud_Grupo s_g=new Solicitud_Grupo(0,
												0,
												(Alumno)peticion.getSession().getAttribute("rolUsuario"),
												(Grupo)Conexion.getInstancia().Buscar(Grupo.class, Integer.parseInt(peticion.getParameter("idGrupo"))),
												new Date());
		Conexion.getInstancia().agregar(s_g);
		peticion.setAttribute("estado","Solicitud enviada correctamente!");
		despachador.forward(peticion, respuesta);
	}
}
