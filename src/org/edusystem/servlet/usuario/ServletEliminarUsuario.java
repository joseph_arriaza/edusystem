package org.edusystem.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Administrador;
import org.edusystem.beans.Profesor;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;

/**
 * Servlet para eliminar un administrado
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEliminarUsuario extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}/**
     * Metodo que elimina el usuario(administrador) de la base de datos segun su id
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		Usuario u=(Usuario)Conexion.getInstancia().Buscar(Usuario.class,Integer.parseInt(peticion.getParameter("idUsurio")));
		System.out.println(peticion.getParameter("idAdmin"));
		if(Integer.parseInt(peticion.getParameter("idUsurio"))==Integer.parseInt(peticion.getParameter("idAdmin"))){
			peticion.setAttribute("estado","No puedes eliminarte a ti mismo!");
		}else{
			try{
				for(Object o:Conexion.getInstancia().hacerConsulta("From Administrador where idUsuario="+u.getIdUsuario())){
					Administrador ad=(Administrador)o;
					Conexion.getInstancia().eliminar(ad);
				}
				Conexion.getInstancia().eliminar(u);
			}catch(Exception exc){}
			Conexion.getInstancia().eliminar(u);
			peticion.setAttribute("estado","Usuario eliminado correctamente!");
		}
		despachador.forward(peticion, respuesta);
	}
}
