package org.edusystem.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Administrador;
import org.edusystem.db.Conexion;


/**
 * Servlet para buscar un usuario
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletBuscarUsuario extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que lista los usuarios encontrados
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("usuario/buscar.jsp");
		peticion.setAttribute("listaUsuariosBusqueda", Conexion.getInstancia().hacerConsulta("From Usuario where nombreUsuario LIKE '%"+peticion.getParameter("txtUsernameBuscar")+"%'"));
		Administrador a=(Administrador)peticion.getSession().getAttribute("rolUsuario");
		despachador.forward(peticion, respuesta);
	}	
}
