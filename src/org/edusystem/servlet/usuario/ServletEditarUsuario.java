package org.edusystem.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;
/**
 * Servlet para editar un usuario
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletEditarUsuario extends HttpServlet{
	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		doPost(peticion,respuesta);
	}
	/**
     * Metodo que edita un usuario con su estado se verifica si esta a editarse, o a llamar el formulario
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException, IOException{
		RequestDispatcher despachador=null;
		Usuario u=(Usuario) Conexion.getInstancia().Buscar(Usuario.class, Integer.parseInt(peticion.getParameter("idUsurio")));
		try{
			if(peticion.getParameter("estadoEU").equals("true")){
				despachador=peticion.getRequestDispatcher("index.jsp");
				u.setContrasenia(peticion.getParameter("txtPassword"));
				Conexion.getInstancia().modificar(u);
				peticion.setAttribute("estado","Usuario modificado!");
			}
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("usuario/editar.jsp");
			peticion.setAttribute("usuarioN",u);
		}
		despachador.forward(peticion, respuesta);
	}
	
}
