package org.edusystem.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.edusystem.beans.Administrador;
import org.edusystem.beans.Alumno;
import org.edusystem.beans.Profesor;
import org.edusystem.beans.Rol;
import org.edusystem.beans.RolUsuario;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;
/**
 * Servlet para registrar un usuario
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletAgregarUsuario extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que registra al usuario, verificando que el nombreUsuario est� disponible, que ningun otro usuario lo haya tomado, que la contrase�a coincida, y segun su tipo de seleccion le da el rol
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		try{
			System.out.println(peticion.getParameter("estadoRUAdmin"));
			if(Conexion.getInstancia().hacerConsulta("From Usuario where nombreUsuario='"+peticion.getParameter("txtNombreUsuario")+"'").size()==0){
				if(peticion.getParameter("txtContrasenia").equals(peticion.getParameter("txtContrasenia2"))){
					Rol rol=(Rol)Conexion.getInstancia().Buscar(Rol.class,1);
					Usuario us=new Usuario(0,rol,peticion.getParameter("txtNombreUsuario"),peticion.getParameter("txtContrasenia"));
					Conexion.getInstancia().agregar(us);
					int idU=0;
					for(Object r:Conexion.getInstancia().hacerConsulta("From Usuario where nombreUsuario='"+peticion.getParameter("txtNombreUsuario")+"'")){
						idU=((Usuario) r).getIdUsuario();
					}
					Administrador adminAdd=new Administrador(0,
															(Usuario)Conexion.getInstancia().Buscar(Usuario.class, idU),
															peticion.getParameter("txtNombre"),
															peticion.getParameter("txtApellido"),
															peticion.getParameter("txtCorreo"),
															peticion.getParameter("txtTelefono"));
					Conexion.getInstancia().agregar(adminAdd);
					peticion.setAttribute("estado","Administrador agregado satisfactoriamente!");
					despachador=peticion.getRequestDispatcher("index.jsp");
				}else{
					peticion.setAttribute("estado","La contrase�a no conincide.");
					despachador=peticion.getRequestDispatcher("usuario/add.jsp");
					peticion.setAttribute("nombreU",peticion.getParameter("txtNombre"));
					peticion.setAttribute("apellidoU",peticion.getParameter("txtApellido"));
					peticion.setAttribute("telefonoU",peticion.getParameter("txtTelefono"));
					peticion.setAttribute("correoU",peticion.getParameter("txtCorreo"));
				}
			}else{
				despachador=peticion.getRequestDispatcher("usuario/add.jsp");
				peticion.setAttribute("nombreU",peticion.getParameter("txtNombre"));
				peticion.setAttribute("apellidoU",peticion.getParameter("txtApellido"));
				peticion.setAttribute("telefonoU",peticion.getParameter("txtTelefono"));
				peticion.setAttribute("correoU",peticion.getParameter("txtCorreo"));
				peticion.setAttribute("estado","Ese nombre de usuario ya esta en uso.");
			}
		}catch(Exception exC){
			despachador=peticion.getRequestDispatcher("usuario/add.jsp");
		}
		
		//RolUsuario rol = null;
		//HttpSession sesion=peticion.getSession(true);	
		despachador.forward(peticion, respuesta);
	}
}
