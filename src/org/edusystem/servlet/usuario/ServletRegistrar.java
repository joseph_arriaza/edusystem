package org.edusystem.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.edusystem.beans.Administrador;
import org.edusystem.beans.Alumno;
import org.edusystem.beans.Profesor;
import org.edusystem.beans.Rol;
import org.edusystem.beans.RolUsuario;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;
/**
 * Servlet para registrar un usuario
 * @author: Halley
 * @version: 20/02/2012/A
 */

public class ServletRegistrar extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que registra al usuario, verificando que el nombreUsuario est� disponible, que ningun otro usuario lo haya tomado, que la contrase�a coincida, y segun su tipo de seleccion le da el rol
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		if(Conexion.getInstancia().hacerConsulta("From Usuario where nombreUsuario='"+peticion.getParameter("txtNombreUsuario")+"'").size()==0){
			if(peticion.getParameter("txtContrasenia").equals(peticion.getParameter("txtContrasenia2"))){
				Rol rol=(Rol)Conexion.getInstancia().Buscar(Rol.class,Integer.parseInt(peticion.getParameter("txtCuenta")));
				Usuario us=new Usuario(0,rol,peticion.getParameter("txtNombreUsuario"),peticion.getParameter("txtContrasenia"));
				Conexion.getInstancia().agregar(us);
				int idU=0;
				for(Object r:Conexion.getInstancia().hacerConsulta("From Usuario where nombreUsuario='"+peticion.getParameter("txtNombreUsuario")+"'")){
					idU=((Usuario) r).getIdUsuario();
				}
				if(peticion.getParameter("txtCuenta").equals("2")){
					Alumno alumnoAdd=new Alumno(0,(Usuario)Conexion.getInstancia().Buscar(Usuario.class, idU),peticion.getParameter("txtNombre"),peticion.getParameter("txtApellido"),peticion.getParameter("txtCorreo"),"default.jpg",peticion.getParameter("txtDireccion"));
					Conexion.getInstancia().agregar(alumnoAdd);
				}else if(peticion.getParameter("txtCuenta").equals("3")){
					Profesor profAdd=new Profesor(0,(Usuario)Conexion.getInstancia().Buscar(Usuario.class, idU),peticion.getParameter("txtNombre"),peticion.getParameter("txtApellido"),peticion.getParameter("txtCorreo"),"default.jpg",peticion.getParameter("txtDireccion"));
					Conexion.getInstancia().agregar(profAdd);
				}
				peticion.setAttribute("estado","");
				despachador=peticion.getRequestDispatcher("ServletAutenticar.do?txtUsuario="+peticion.getParameter("txtNombreUsuario")+"&txtPassword="+peticion.getParameter("txtContrasenia")+"&ruta="+peticion.getParameter("ruta"));
			}else{
				peticion.setAttribute("estado","La contrase�a no conincide.");
				despachador=peticion.getRequestDispatcher("index.jsp");
			}
		}else{
			despachador=peticion.getRequestDispatcher("index.jsp");
			peticion.setAttribute("nombreU",peticion.getParameter("txtNombre"));
			peticion.setAttribute("apellidoU",peticion.getParameter("txtApellido"));
			peticion.setAttribute("direccionU",peticion.getParameter("txtDireccion"));
			peticion.setAttribute("correoU",peticion.getParameter("txtCorreo"));
			peticion.setAttribute("estado","Ese nombre de usuario ya esta en uso.");
		}
		//RolUsuario rol = null;
		//HttpSession sesion=peticion.getSession(true);	
		despachador.forward(peticion, respuesta);
	}
}
