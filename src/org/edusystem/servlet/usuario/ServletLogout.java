package org.edusystem.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.edusystem.beans.RolUsuario;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;

/**
 * Servlet para cerrar la sesi�n
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletLogout extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que finaliza la sesi�n del usuario y todos sus parametros son invalidados
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		peticion.getSession(false);
		peticion.getSession().invalidate();
		peticion.getSession().removeAttribute("usuario");
		peticion.getSession().removeAttribute("rolUsuario");
		despachador.forward(peticion, respuesta);
	}
}
