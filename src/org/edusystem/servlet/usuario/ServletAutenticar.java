package org.edusystem.servlet.usuario;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.edusystem.beans.Administrador;
import org.edusystem.beans.Alumno;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Profesor;
import org.edusystem.beans.RolUsuario;
import org.edusystem.beans.Usuario;
import org.edusystem.db.Conexion;

/**
 * Servlet para autenticar el usuario
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class ServletAutenticar extends HttpServlet{

	/**
     * Metodo que manda recibe el parametros por get y los envia a post
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	/**
     * Metodo que busca la lista de los grupos a los que pertenece cierto alumno
     * @param Alumno es el alumno del que se buscar�n los grupos
     * @return Devuelve la lista de grupos a los que pertenece el alumno
     */
	public ArrayList<Grupo> getGrupo(Alumno al){
		ArrayList<Grupo> listaGrupos=new ArrayList<Grupo>();
		for(Object o:Conexion.getInstancia().hacerConsulta("From Grupo")){
			Grupo g=(Grupo) o;
			try{
				String idAlumnosGrupoS[]=g.getIdAlumno().split(" ");
				for(String idAlumno:idAlumnosGrupoS){
					if(idAlumno.equals(""+al.getIdAlumno())){
						listaGrupos.add(g);
					}
				}
			}catch(Exception ex){}
		}
		return listaGrupos;
	}
	
	/**
     * Metodo que realiza la autenticaci�n del usuario, verifica en la base de datos y verifica que rol tiene para ser instanciado con una clase heredada y crea la sesi�n con la ruta del proyecto y con el objeto usuario y el objeto segun su rol
     * @param peticion Son los atributos que se han dado en la clase anterior
     * @param respuesta Son los aspectos de como esta dise�ada la pagina
     */
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		if(Conexion.getInstancia().autenticarUsuario(peticion.getParameter("txtUsuario"), peticion.getParameter("txtPassword"))!=null){
			Usuario usr=Conexion.getInstancia().autenticarUsuario(peticion.getParameter("txtUsuario"), peticion.getParameter("txtPassword"));
			RolUsuario rol = null;
			HttpSession sesion=peticion.getSession(true);
			if(usr.getRol().getIdRol()==1){
				System.out.println(usr.getIdUsuario());
				for(Object recorrer:Conexion.getInstancia().hacerConsulta("From Administrador where idUsuario="+usr.getIdUsuario())){
					System.out.println(usr.getRol().getIdRol());
					Administrador ad=(Administrador) recorrer;
					rol=new Administrador(ad.getIdAdministrador(),ad.getUsuario(),ad.getNombreAdministrador(),ad.getApellidoAdministrador(),ad.getCorreoAdministrador(),ad.getTelefonoAdministrador());
					System.out.println(usr.getRol().getIdRol());
					sesion.setAttribute("buscarExamenPts","false");
				}				
			}else if(usr.getRol().getIdRol()==2){
				for(Object recorrer:Conexion.getInstancia().hacerConsulta("From Alumno where idUsuario="+usr.getIdUsuario())){
					Alumno al=(Alumno) recorrer;
					rol=new Alumno(al.getIdAlumno(),al.getUsuario(),al.getNombreAlumno(),al.getApellidoAlumno(),al.getCorreoAlumno(),al.getFotoAlumno(),al.getDireccionAlumno());
					sesion.setAttribute("listaGrupoAl",getGrupo(al));
					sesion.setAttribute("buscarExamenPts","true");
				}
			}else if(usr.getRol().getIdRol()==3){
				for(Object recorrer:Conexion.getInstancia().hacerConsulta("From Profesor where idUsuario="+usr.getIdUsuario())){
					Profesor p=(Profesor) recorrer;
					rol=new Profesor(p.getIdProfesor(),p.getUsuario(),p.getNombreProfesor(),p.getApellidoProfesor(),p.getCorreoProfesor(),p.getFotoProfesor(),p.getDireccionProfesor());
					sesion.setAttribute("listaGrupo", Conexion.getInstancia().hacerConsulta("From Grupo WHERE idProfesor="+p.getIdProfesor()));
					sesion.setAttribute("buscarExamenPts","false");
				}
			}
			String rutaTemp=this.getServletContext().getRealPath("");
			sesion.setAttribute("ruta",rutaTemp.replace('\\','/'));
			System.out.println(rutaTemp);
			sesion.setAttribute("rolUsuario",rol);
			sesion.setAttribute("usuario", usr);
			sesion.setAttribute("ruta",peticion.getParameter("ruta"));
			despachador=peticion.getRequestDispatcher("usuario/dashboard.jsp");
		}else{
			peticion.setAttribute("usuarioTxt",peticion.getParameter("txtUsuario"));
			peticion.setAttribute("estado", "Error al iniciar sesion");
			despachador=peticion.getRequestDispatcher("index.jsp");
		}		
		despachador.forward(peticion, respuesta);
	}
}
