package org.edusystem.utilidad;

import java.util.ArrayList;
import java.util.List;

import org.edusystem.beans.Curso;
import org.edusystem.beans.Examen;
import org.edusystem.beans.Grupo;
import org.edusystem.beans.Nota;
import org.edusystem.beans.Pregunta;
import org.edusystem.beans.Tema;
import org.edusystem.db.Conexion;
/**
 * Esta clase tiene la funci�n de eliminar en cascada
 * @author: Halley
 * @version: 20/02/2012/A
 */
public class CascadeDelete {
	public static final CascadeDelete instancia= new CascadeDelete();

	/**
     * Metodo que nos obtiene una lista de los cursos segun el grupo
     * @param idGrupo El id del grupo para buscar sus cursos
     * @return La lista de Cursos que tienen dicho id
     */
	
	public List<Object> getIdCurso(int idGrupo){
		return Conexion.getInstancia().hacerConsulta("From Curso where grupo="+idGrupo);
	}

	/**
     * Metodo que nos obtiene una lista de los tema segun el curso
     * @param idCurso El id del curso para buscar sus temas
     * @return La lista de Temas que tienen dicho id
     */
	
	public List<Object> getIdTema(int idCurso){
		return Conexion.getInstancia().hacerConsulta("From Tema where curso="+idCurso);
	}
	/**
     * Metodo que nos obtiene una lista de los examenes segun el tema
     * @param idTema El id del tema para buscar sus examenes
     * @return La lista de Examenes que tienen dicho id
     */
	public List<Object> getIdExamen(int idTema){
		return Conexion.getInstancia().hacerConsulta("From Examen where idTema="+idTema);
	}
	/**
     * Metodo que nos obtiene una lista de las preguntas segun el examen
     * @param idExamen El id del examen para buscar sus preguntas
     * @return La lista de Pregunats que tienen dicho id
     */
	public List<Object> getIdPregunta(int idExamen){
		return Conexion.getInstancia().hacerConsulta("From Pregunta where examen="+idExamen);
	}
	/**
     * Metodo que nos obtiene una lista de las notas segun el examen
     * @param idExamen El id del examen para buscar sus notas
     * @return La lista de Notas que tienen dicho id
     */	
	public List<Object> getIdNota(int idExamen){
		return Conexion.getInstancia().hacerConsulta("From Nota where idExamen="+idExamen);
	}
	/**
     * Metodo que nos obtiene una lista de las notas segun el examen y el alumno
     * @param idExamen El id del examen para buscar sus notas
     * @param idAlumno El id del alumno para buscar sus notas
     * @return La lista de Notas que tienen dicho id de alumno y de examen
     */
	public List<Object> getIdNota(int idExamen,int idAlumno){
		return  Conexion.getInstancia().hacerConsulta("From Nota where idExamen="+idExamen+" and idAlumno="+idAlumno);
	}
	/**
     * Metodo que elimina en la nota
     * @param p Objeto para obtener su id y buscar los objetos dependientes de esta entidad, y asi mismo eliminarlos
     * @return El estado de si se elimin� o no
     */
	public boolean delNota(Examen e){
		try{
			for(Object nO:getIdNota(e.getIdExamen())){
				Nota n=(Nota)nO;
				//examen
				eliminar(n);
			}
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	/**
     * Metodo que elimina en la pregunta
     * @param e Objeto para obtener su id y buscar los objetos dependientes de esta entidad, y asi mismo eliminarlos
     * @return El estado de si se elimin� o no
     */
	public boolean delPregunta(Examen e){
		try{
			for(Object pO:getIdPregunta(e.getIdExamen())){
				Pregunta p=(Pregunta) pO;
				eliminar(p);
			}
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	/**
     * Metodo que elimina en el examen
     * @param t Objeto para obtener su id y buscar los objetos dependientes de esta entidad, y asi mismo eliminarlos
     * @return El estado de si se elimin� o no
     */
	public boolean delExamen(Tema t){
		try{
			for(Object eO:getIdExamen(t.getIdTema())){
				Examen e=(Examen) eO;
				delPregunta(e);
				delNota(e);
				eliminar(e);
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}
	/**
     * Metodo que elimina en el tema
     * @param c Objeto para obtener su id y buscar los objetos dependientes de esta entidad, y asi mismo eliminarlos
     * @return El estado de si se elimin� o no
     */
	public boolean delTema(Curso c){
		try{
			for(Object tO:getIdTema(c.getIdCurso())){
				Tema t=(Tema) tO;
				delExamen(t);
				eliminar(t);
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}
	/**
     * Metodo que elimina en el curso
     * @param idGrupo  Id para buscar los objetos dependientes de esta entidad, y asi mismo eliminarlos
     * @return El estado de si se elimin� o no
     */
	public boolean delCurso(int idGrupo){
		try{
			for(Object cO:getIdCurso(idGrupo)){
				Curso c=(Curso) cO;
				delTema(c);
				eliminar(c);
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}
	/**
     * Metodo que elimina el grupo
     * @param idGrupo para buscar los objetos dependientes de esta entidad, y asi mismo eliminarlos
     * @return El estado de si se elimin� o no
     */
	public boolean delAll(int idGrupo){
		boolean estado=false;
		try{
			//idCurso,idTema,idExamen,idPregunta,idNota=0
			Grupo g=new Grupo();
			g.setIdGrupo(idGrupo);
			delCurso(idGrupo);
			eliminar(g);
			estado=true;
		}catch(Exception e){}
		return estado;
	}
	/**
     * Metodo que elimina de la base de datos
     * @param O objeto que se eliminar� de la base de datos
     */
	public void eliminar(Object o){
		Conexion.getInstancia().eliminar(o);
	}
}